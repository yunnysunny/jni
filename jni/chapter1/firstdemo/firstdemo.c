#include <stdio.h>
#include "com_whyun_jni_chapter1_FirstDemo.h"

/*
 * Class:     com_whyun_jni_chapter1_FirstDemo
 * Method:    getNum
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_whyun_jni_chapter1_FirstDemo_getNum
  (JNIEnv *env, jobject obj) {
	  return (jint)1;
}

/*
 * Class:     com_whyun_jni_chapter1_FirstDemo
 * Method:    getString
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_whyun_jni_chapter1_FirstDemo_getString
  (JNIEnv *env, jobject ob) {
	  jstring jinfo			= (*env)->NewStringUTF(env,"the first demo.");
	  return jinfo;
}