package com.whyun.jni.chapter1;

/**
 * User: sunny
 * Date: 15-10-28
 * Time: 下午12:29
 */
public class FirstDemo {
    static{
        System.loadLibrary("firstdemo");
    }
    public native int getNum();
    public native String getString();
    public static void main(String []args) {
        FirstDemo demo = new FirstDemo();
        System.out.println("num:"+demo.getNum()+",string:"+demo.getString());
    }
}
