package com.whyun.jni.bean;

/**
 * The Class UserBean.
 */
public final class UserBean {
	
	/** The age. */
	private int age;
	
	/** The name. */
	private String name;
	
	/** The sex. */
	private int sex;
	
	/**
	 * Gets the age.
	 *
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	
	/**
	 * Sets the age.
	 *
	 * @param age the new age
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the sex.
	 *
	 * @return the sex
	 */
	public int getSex() {
		return sex;
	}
	
	/**
	 * Sets the sex.
	 *
	 * @param sex the new sex
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}
	
}
