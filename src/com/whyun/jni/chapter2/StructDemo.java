package com.whyun.jni.chapter2;

import java.util.ArrayList;

import com.whyun.jni.bean.UserBean;

public class StructDemo {
	static {
		System.loadLibrary("chapter2");
	}
	
	public native int getSum(int a, int b);
	public native int getSum(byte [] array);
	public native ArrayList<UserBean> getUserList(int num);
	public native void showException() throws Exception;

	public static void main(String[] args) {
		StructDemo demo = new StructDemo();
		int sum =demo.getSum(1,2);
		System.out.println("sum1:"+sum);
		sum = demo.getSum(new byte[]{1,2,3,4});
		System.out.println("sum2:"+sum);
		ArrayList<UserBean> list = demo.getUserList(3);
		for(UserBean bean:list) {
			System.out.println("age:"+bean.getAge()+",name:"+bean.getName());
		}
		try {
			demo.showException();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
